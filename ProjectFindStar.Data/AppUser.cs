﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace ProjectFindStar.Data
{
	public class AppUser : IdentityUser<string>
    {
        [Required]
        public string PersonalName { get; set; }
    }
}
