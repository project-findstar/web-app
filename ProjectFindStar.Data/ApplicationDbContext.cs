﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ProjectFindStar.Data
{
    public class ApplicationDbContext :
        IdentityDbContext<AppUser, IdentityRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder
                .Entity<PointOfInterest>()
                .HasIndex(
                    nameof(PointOfInterest.Lattitude),
                    nameof(PointOfInterest.Longitude)
                );
        }

        public DbSet<PointOfInterest> PointsOfInterest { get; protected set; }
    }
}
