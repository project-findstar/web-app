﻿namespace ProjectFindStar.Data
{
    public interface ISituated
    {
        double Lattitude { get; set; }
        double Longitude { get; set; }
    }
}
