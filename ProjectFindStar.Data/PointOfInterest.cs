﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectFindStar.Data
{
    public class PointOfInterest : ISituated
    {
        [Key]
        public int Id { get; set; }

        public double Lattitude { get; set; }
        public double Longitude { get; set; }

        public string Name { get; set; }
        public Guid MapFormId { get; set; }
    }
}
