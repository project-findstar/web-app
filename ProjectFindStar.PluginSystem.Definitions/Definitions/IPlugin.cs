﻿namespace ProjectFindStart.PluginSystem.Definitions
{
    public interface IPlugin
    {
        string UniqueIdentifier { get; }

    }
}
