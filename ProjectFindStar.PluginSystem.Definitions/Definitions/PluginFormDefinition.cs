﻿using System.Collections;
using System.Collections.Generic;

namespace ProjectFindStart.PluginSystem.Definitions
{
    public class PluginFormDefinition : IEnumerable<PluginInputDefinition>
    {
        public List<PluginInputDefinition> InputDefinitions { get; }
            = new List<PluginInputDefinition>();


        public void Add(PluginInputDefinition definition) => 
            InputDefinitions.Add(definition);
        
        public IEnumerator<PluginInputDefinition> GetEnumerator() =>
            InputDefinitions.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() =>
            InputDefinitions.GetEnumerator();
    }
}
