﻿using System;

namespace ProjectFindStart.PluginSystem.Definitions
{
    public class PluginInputDefinition
    {
        public string Name { get; }
        public string InputType { get; }
        public string Value { get; set; }
        public string Placeholder { get; set; }

        public PluginInputDefinition(string name, string inputType)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));
            if (inputType == null)
                throw new ArgumentNullException(nameof(inputType));
            Name = name;
            InputType = inputType;
        }
    }
}