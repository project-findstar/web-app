﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectFindStar.PluginSystem.Definitions.Definitions
{
    class PluginPartialDefinition
    {
        public string Area { get; }
        public string Controller { get; }
    }
}
