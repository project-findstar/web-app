﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectFindStart.PluginSystem.Definitions
{
    public interface PointOfInterestTypeDefinition
    {
        string Name { get; }
        string Description { get; }
        string IconUrl { get; }

        PluginFormDefinition MapForm { get; }
    }
}
