﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectFindStar.Data;
using ProjectFindStar.Services;

namespace ProjectFindStar.Areas.Admin.Controllers
{
    [Area(nameof(Admin))]
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext context;
        private readonly ILogger logger;
        public HomeController(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            this.ViewData["ActivePage"] = "index";
            return this.View();
        }

        public IActionResult Users()
        {
            this.ViewData["ActivePage"] = "users";
            DbSet<AppUser> users = this.context.Users;
            return this.View(users);
        }
    }
}