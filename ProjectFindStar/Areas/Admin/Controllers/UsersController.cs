﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ProjectFindStar.Areas.Admin.Models;
using ProjectFindStar.Areas.Admin.Services;
using ProjectFindStar.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectFindStar.Areas.Admin.Controllers
{
    [Area(nameof(Admin))]
    [Authorize]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<AppUser> userManager;
        private readonly UsersService service;

        public UsersController(ApplicationDbContext context, UserManager<AppUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.service = new UsersService(userManager);
        }

        public async Task<IActionResult> Index()
        {
        
            this.ViewData["ActivePage"] = "users";
            List<UserViewModel> users = await service.ParseUsersAsync(this.context.Users);
            return this.View(users);
        }

        public async Task<IActionResult> Delete(string id)
        {
            AppUser user = await this.userManager.FindByIdAsync(id);
            await this.userManager.UpdateSecurityStampAsync(user);
            if (user == await this.userManager.GetUserAsync(HttpContext.User))
            {
                foreach (var cookie in Request.Cookies.Keys)
                {
                    Response.Cookies.Delete(cookie);
                }
            }
            await this.userManager.DeleteAsync(user);
            return this.Redirect("/Admin/Users");
        }
    }
}