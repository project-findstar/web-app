﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using ProjectFindStar.Areas.Admin.Models;
using ProjectFindStar.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectFindStar.Areas.Admin.Services
{
    public class UsersService
    {
        private readonly UserManager<AppUser> manager;

        public UsersService(UserManager<AppUser> manager)
        {
            this.manager = manager;
        }
        public async Task<List<UserViewModel>> ParseUsersAsync(DbSet<AppUser> users)
        {
            List<UserViewModel> viewModels = new List<UserViewModel>();
            foreach (AppUser user in users)
            {
                if (await manager.IsInRoleAsync(user, "Admin"))
                {
                    continue;
                }
                UserViewModel uvm = new UserViewModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    Name = user.PersonalName
                };
                viewModels.Add(uvm);
            }

            return viewModels;
        }
    }
}

