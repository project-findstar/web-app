﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(ProjectFindStar.Areas.Identity.IdentityHostingStartup))]
namespace ProjectFindStar.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}