﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectFindStar.Services
{
    public class AppConfiguration : IAppConfiguration
    {
        public string AdminUserEmail => "find@star.com";
        public string AdminUserDefaultPassword => "123456";

        public IReadOnlyList<string> RoleNames { get; } = new[] {
            "Admin"
        };

        public string AdminRoleName => "Admin";
    }
}
