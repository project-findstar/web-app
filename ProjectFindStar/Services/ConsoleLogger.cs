﻿using System;
using System.Collections.Generic;

namespace ProjectFindStar.Services
{
    public class ConsoleLogger : ILogger
    {
        public ConsoleColor DefaultColor { get; }

        public string DefaultTitle { get; }

        public ConsoleLogger(ConsoleColor defaultColor, string defaultTitle)
        {
            if (defaultTitle == null)
                throw new ArgumentNullException(nameof(defaultTitle));
            DefaultColor = defaultColor;
            DefaultTitle = defaultTitle;
        }
        
        public ILogger Log(ConsoleColor color, string title, string message)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(
                $"[{DateTime.Now.ToShortDateString()}] " +
                $"[{DateTime.Now.ToShortTimeString()}]\n" +
                $"{title}: {message}"
            );
            Console.ResetColor();
            return this;
        }
    }
}
