﻿using Microsoft.AspNetCore.Identity;
using ProjectFindStar.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectFindStar.Services
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        readonly IServiceProvider serviceProvider;
        readonly UserManager<AppUser> userManager;
        readonly RoleManager<IdentityRole> roleManager;
        readonly IAppConfiguration appConfiguration;

        public DatabaseInitializer(
            IServiceProvider serviceProvider,
            UserManager<AppUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IAppConfiguration appConfiguration
        )
        {
            this.serviceProvider = serviceProvider;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.appConfiguration = appConfiguration;
        }

        public async Task InitializeAsync()
        {

            foreach (object roleName in appConfiguration.RoleNames)
            {
                if (!await roleManager.RoleExistsAsync(roleName.ToString()))
                {
                    await roleManager.CreateAsync(new IdentityRole(roleName.ToString()));
                }
            }

            var adminUser = new AppUser {
                Email = appConfiguration.AdminUserEmail,
                UserName = appConfiguration.AdminUserEmail,
                EmailConfirmed = true
            };

            if (await userManager.FindByNameAsync(adminUser.UserName) == null)
            {
                await userManager.CreateAsync(adminUser, appConfiguration.AdminUserDefaultPassword);
            }

            adminUser = await userManager.FindByNameAsync(adminUser.UserName);

            await userManager.AddToRoleAsync(adminUser, appConfiguration.AdminRoleName);
            await Task.Yield();
        }
    }

}
