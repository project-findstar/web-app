﻿using Microsoft.AspNetCore.Identity.UI.Services;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ProjectFindStar.Services
{
    public class EmailSender : IEmailSender
    {
        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("donotreply@findstar.online");
                mailMessage.To.Add(new MailAddress(email));
                mailMessage.Subject = subject;
                mailMessage.Body = htmlMessage;
                mailMessage.IsBodyHtml = true;
                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.UseDefaultCredentials = false;
                    NetworkCredential credentials = new NetworkCredential
                    {
                        UserName = "findstar@abv.bg",
                        Password = "findstar1"
                    };
                    smtp.Credentials = credentials;
                    smtp.Host = "smtp.abv.bg";
                    smtp.Port = 465;
                    smtp.EnableSsl = true;

                    await smtp.SendMailAsync(mailMessage);
                }
            }            
        }
    }
}
