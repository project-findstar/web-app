﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectFindStar.Services
{
    public interface IAppConfiguration
    {
        string AdminUserEmail { get; }
        string AdminUserDefaultPassword { get; }
        IReadOnlyList<String> RoleNames { get; }
        string AdminRoleName { get; }
    }

}
