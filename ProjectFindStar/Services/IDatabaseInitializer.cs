﻿using System.Threading.Tasks;

namespace ProjectFindStar.Services
{
    public interface IDatabaseInitializer
    {
        Task InitializeAsync();
    }
}
