﻿using System;

namespace ProjectFindStar.Services
{
    public interface ILogger
    {
        ConsoleColor DefaultColor { get; }
        string DefaultTitle { get; }
        ILogger Log(ConsoleColor color, string title, string message);
    }

    public static class Extensions_ILogger
    {
        public static ILogger Log(this ILogger logger, string message) =>
            logger.Log(logger.DefaultColor, logger.DefaultTitle, message);
        public static ILogger Log(this ILogger logger, string title, string message) =>
            logger.Log(logger.DefaultColor, title, message);
        public static ILogger Log(this ILogger logger, ConsoleColor color, string message) => 
            logger.Log(color, logger.DefaultTitle, message);
    }
}
