﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectFindStar.Services
{
    public class LoggerSet : ILogger, ILoggerCache
    {
        public LoggerSet(int maxCacheSize, params ILogger[] loggerParams) :
            this(maxCacheSize, loggers: loggerParams)
        { }

        public LoggerSet(int maxCacheSize, IEnumerable<ILogger> loggers)
        {
            Loggers = loggers;
            MaxCacheSize = maxCacheSize;
            if (loggers == null) throw new ArgumentNullException(nameof(loggers));
            if (!loggers.Any()) throw new ArgumentException("No loggers");
        }


        Queue<string> cache = new Queue<string>();
        public ConsoleColor DefaultColor => Loggers.First().DefaultColor;

        public string DefaultTitle => Loggers.First().DefaultTitle;

        public IEnumerable<ILogger> Loggers { get; }
        public int MaxCacheSize { get; }

        public string[] GetCacheSnapshot() => cache.ToArray();



        public ILogger Log(ConsoleColor color, string title, string message)
        {
            var time = DateTime.Now;
            cache.Enqueue(
                $"[{time.ToShortDateString()}] " +
                $"[{time.ToShortTimeString()}]\n" +
                $"{title}:\n" +
                $"{message}"
            );

            while (cache.Count > MaxCacheSize) cache.Dequeue();

            foreach(ILogger logger in Loggers)
            {
                logger.Log(color, title, message);
            }

            return this;
        }
    }
}
