﻿using ProjectFindStart.PluginSystem.Definitions;
using System;
using System.Collections.Generic;

namespace ProjectFindStar.Services.PluginServices
{
    interface IFormDefinitionRepository
    {
        PluginFormDefinition GetFormDefinition(Guid guid);
        IEnumerable<KeyValuePair<Guid, PluginFormDefinition>> GetDefinitions();
    }
}
