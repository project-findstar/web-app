﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using ProjectFindStart.PluginSystem.Definitions;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectFindStart.PluginSystem.HtmlHelpers
{
    [HtmlTargetElement("form", TagStructure = TagStructure.NormalOrSelfClosing)]
    public class PluginFormHelper : TagHelper
    {
        [HtmlAttributeName("plugin-definition")]
        public PluginFormDefinition FormDefinition { get; set; }

        [HtmlAttributeName(DictionaryAttributePrefix ="class-for-input-oftype-")]
        public Dictionary<string, string> StylesPerInputType { get; set; } =
            new Dictionary<string, string>();

        public override void Init(TagHelperContext context) => base.Init(context);
        public override async Task ProcessAsync(
            TagHelperContext context,
            TagHelperOutput output
        )
        {
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Content.SetHtmlContent(BuildInnerHtml(FormDefinition));
            await Task.Yield();
        }

        string BuildInnerHtml(PluginFormDefinition form)
        {
            var builder = new StringBuilder();
            foreach (PluginInputDefinition input in FormDefinition)
            {
                builder.Append($"<input ");
                builder.Append($"name='{input.Name}' ");
                builder.Append($"type='{input.InputType}' ");
                if (input.Value != null)
                {
                    builder.Append($"value='{input.Value}'");
                }
                if (input.Placeholder != null)
                {
                    builder.Append($"placeholder='{input.Placeholder}' ");
                }
                builder.Append("/>\n");
            }
            return builder.ToString();
        }

    }
}
